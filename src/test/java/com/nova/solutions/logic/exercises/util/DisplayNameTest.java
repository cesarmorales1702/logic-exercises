package com.nova.solutions.logic.exercises.util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Prueba de etiquetado de test, se agrega un identificador unico a cada test")
public class DisplayNameTest {
  
  @Test
  @DisplayName("prueba 1")
  public void test1() {
    System.out.println("test 1");
  }
  
  @Test
  @DisplayName("prueba 2")
  public void test2() {
    System.out.println("test 2");
  }
  
  @Test
  @DisplayName("prueba 3")
  public void test3() {
    System.out.println("test 3");
  }
}
