package com.nova.solutions.logic.exercises.util;

import com.nova.solutions.logic.exercises.model.PrimeNumber;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.time.Duration;

public class AssertionsTest {

  @Test
  @DisplayName("Ejemplo de test de una suma con un resultado ok")
  public void assertAddUp() throws Exception {
    String num1 = "10";
    String num2 = "14";
    Assertions.assertEquals(24, Util.addUp(num1, num2));
  }

  @Test
  @DisplayName("Ejemplo de test de una suma con un resultado no ok")
  public void assertAddUpBad() throws Exception {
    String num1 = "18";
    String num2 = "A";

    Assertions.assertThrows(Exception.class, () -> Util.addUp(num1, num2));
  }

  @Test
  @DisplayName("Ejemplo de test de un bloque de asserts que un test debe cumplir para ser ok")
  public void assertBlock() {
    Integer num = 50;
    PrimeNumber number = Util.getPrimeNumber(num);
    Assertions.assertAll("number", () -> Assertions.assertEquals(50, number.getNumber()),
        () -> Assertions.assertEquals(false, number.isPrime()),
        () -> Assertions.assertAll("dividers",
            () -> Assertions.assertEquals(1, number.getDividers().get(0)),
            () -> Assertions.assertEquals(2, number.getDividers().get(1)),
            () -> Assertions.assertEquals(5, number.getDividers().get(2)),
            () -> Assertions.assertEquals(10, number.getDividers().get(3)),
            () -> Assertions.assertEquals(25, number.getDividers().get(4)),
            () -> Assertions.assertEquals(50, number.getDividers().get(5))));
  }
  
  @Test
  @DisplayName("Ejemplo de timeout ok")
  public void timeOutNotExceededTest() {
    Assertions.assertTimeout(Duration.ofSeconds(2), () -> Util.complete(1));
  }
  
  @Test
  @DisplayName("Ejemplo de timeout ok con respuesta")
  public void timeOutNotExceededWithResultTest() {
    PrimeNumber number = Assertions.assertTimeout(Duration.ofMinutes(2), ()-> Util.process(1, 27));
    Assertions.assertNotNull(number);
  }
  
  
}
